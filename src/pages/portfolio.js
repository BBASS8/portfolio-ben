import React from "react";
import Layout from "../components/Layout";
import * as styles from "../styles/portfolio.module.css";

const Portfolio = () => {
  const projects = [
    {
      title: "Health Hounds",
      videoUrl: "https://player.vimeo.com/video/849508483",
      description:
        "Health Hounds is a fitness app built on the idea that busy or inexperienced users want to exercise, but don't know where to start or what to do",
    },
  ];

  return (
    <>
      <Layout />
      <div className={styles.container}>
        <h1 className={styles.center}>Portfolio</h1>
        <ul>
          {projects.map((project, index) => (
            <li key={index} className={styles.projectitem}>
              <h2>{project.title}</h2>
              <div className={styles.videocontainer}>
                <iframe
                  src={project.videoUrl}
                  title={project.title}
                  width="640"
                  height="360"
                  frameBorder="0"
                  allow="autoplay; fullscreen"
                  allowFullScreen
                ></iframe>
              </div>
              <p>{project.description}</p>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default Portfolio;
