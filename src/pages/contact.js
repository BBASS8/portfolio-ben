import React, { useRef, useState } from "react";
import Layout from "../components/Layout.js";
import emailjs from "@emailjs/browser";
import * as styles from "../styles/contact.module.css";

const ContactUs = () => {
  const form = useRef();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };
  const handleLEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };
  const handleMessageChange = (event) => {
    const value = event.target.value;
    setMessage(value);
  };

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_kxbb883",
        "template_irtq1gs",
        form.current,
        "kkEpu8T2ZddSnQ91M"
      )
      .then(
        (result) => {
          console.log(result.status);
          console.log(result.text);
          setIsSubmitted(true);
          setName("");
          setEmail("");
          setMessage("");
          window.location.replace("/thankyou");
        },
        (error) => {
          console.log(error.text);
        }
      );
  };

  return (
    <>
      <Layout />
      <div className={styles.contact}>
        <form ref={form} onSubmit={sendEmail}>
          <label className={styles.label}>
            Name
            <input
              type="text"
              name="user_name"
              value={name}
              onChange={handleNameChange}
              className={styles.inputfield}
            />
          </label>
          <label>
            Email
            <input
              type="email"
              name="user_email"
              value={email}
              onChange={handleLEmailChange}
              className={styles.inputfield}
            />
          </label>
          <label>
            Message
            <textarea
              name="message"
              value={message}
              onChange={handleMessageChange}
              className={styles.inputfield}
            />
          </label>
          <input type="submit" value="Send" className={styles.btn} />
        </form>
      </div>
      <div>
        {isSubmitted && (
          <p className={styles.suc}>Message Sent Successfully!</p>
        )}
      </div>
    </>
  );
};

export default ContactUs;
