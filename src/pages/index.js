import * as React from "react";
import Layout from "../components/Layout";
import * as styles from "../styles/home.module.css";
import Croppedpic from "../images/Croppedpic.png";

const IndexPage = () => {
  return (
    <Layout>
      <main>
        <header>
          <div className={styles.centeredContent}>
            <h1 className={styles.title}>Benjamin Bass</h1>
            <p className={styles.description}>Software Developer</p>
          </div>
        </header>
        <div className={styles.image}>
          <img className={styles.littleben} src={Croppedpic} alt="Year 2017" />
        </div>
      </main>
    </Layout>
  );
};

export default IndexPage;

export const Head = () => <title>Home Page</title>;
