import React from "react";
import Layout from "../components/Layout";
import * as styles from "../styles/thankyou.module.css";

const Thankyou = () => {
  return (
    <Layout>
      <div className={styles.thankyoucontainer}>
        <h2>Thank You for Messaging Me!</h2>
        <p>Your message has been received.</p>
      </div>
    </Layout>
  );
};

export default Thankyou;
