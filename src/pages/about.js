import React from "react";
import Nav from "../components/Navbar";
import * as styles from "../styles/about.module.css";

const About = () => {
  return (
    <>
      <Nav />
      <body>
        <header>
          <h1 className={styles.title}>Welcome to my about me page!</h1>
        </header>

        <main>
          <div className={styles.timeline}>
            <div className={`${styles.container} ${styles.left}`}>
              <div className={styles.content}>
                <h2>2019</h2>
                <p>
                  My name is Ben Bass, I am a software developer from New
                  Jersey, however I have not always been an engineer. I received
                  my bachelor's degree from the University of Delaware where I
                  majored in Health Behavior Science and Public Health. My goal
                  was to help people improve their health to ultimately provide
                  them with a better quality of life.
                </p>
              </div>
            </div>
            <div className={`${styles.container} ${styles.right}`}>
              <div className={styles.content}>
                <h2>2020</h2>
                <p>
                  {" "}
                  I went on to work in substance abuse treatment for 3 years
                  where I worked on a daily basis to help others and be of
                  service to the best of my ability to those struggling in the
                  grips of addiction. The best feeling in the world was seeing
                  patients come back to visit me months after treatment, and for
                  me to see the change in their lives, and knowing that I had a
                  small but meaningful role in their healing process.
                </p>
              </div>
            </div>
            <div className={`${styles.container} ${styles.left}`}>
              <div className={styles.content}>
                <h2>2023</h2>
                <p>
                  After a wonderful 3 years I had a realization that I had
                  become too comfortable at my job. Everything came easily. I
                  had mastered a job that had once been so challenging. This was
                  the catalyst which inspired me to walk a new path.{" "}
                </p>
              </div>
            </div>
            <div className={`${styles.container} ${styles.right}`}>
              <div className={styles.content}>
                <h2>2023</h2>
                <p>
                  I tried my hand at software engineering and I came to love the
                  process of learning, experimenting, solving, and building. I
                  subsequently enrolled in an immersive software engineering
                  bootcamp where I honed my skills and fell in love with the
                  coding community.
                </p>
              </div>
            </div>
            <div className={`${styles.container} ${styles.left}`}>
              <div className={styles.content}>
                <h2>Present Day</h2>
                <p>
                  I not only created web applications to solve real world
                  problems in the bootcamp, but I was able to begin working as a
                  software engineering intern at a start-up called Neurability
                  Technologies following my graduation. Luckily this company I
                  have so gratefully landed at supports a mission that agrees
                  with my values - to help those in need. Neurability develops
                  solutions that empower organizations to experience the
                  transformative impact of cognitive diversity. I have realized
                  that my journey has come full circle, in that I have found an
                  avenue to continue being of service to the world by making a
                  positive impact.
                </p>
              </div>
            </div>
          </div>

          <div className={styles.centerContainer}>
            <div className={styles.contact}>
              <a href="/contact">Contact Me</a>
            </div>
            <div className={styles.linkedin}>
              <a href="https://www.linkedin.com/in/ben-bass8/">
                LinkedIn Profile
              </a>
            </div>
          </div>
        </main>
      </body>
    </>
  );
};

export default About;
