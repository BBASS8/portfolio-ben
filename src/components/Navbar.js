import React from "react";

class Nav extends React.Component {
  render() {
    return (
      <nav className="navbar">
        <div className="container">
          <a href="/">Home</a>
          <a href="/about">About</a>
          <a href="/portfolio">Portfolio</a>
          <a href="/contact">Contact</a>
        </div>
      </nav>
    );
  }
}

export default Nav;
