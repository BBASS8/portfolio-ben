import React from "react";
import Nav from "./Navbar";
import "../styles/global.css";

export default function Layout({ children }) {
  return (
    <>
      <Nav />
      <div className="layout">
        <div className="content">{children}</div>
        <footer>
          <p>Copyright 2023 Ben Bass</p>
        </footer>
      </div>
    </>
  );
}
